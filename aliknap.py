# Aliknap is a simple aid in converting Alitalia miles in e-coupons 
# Copyright (C) 2012 by Mattia Monga <monga@debian.org> 
# Most of the code is taken from Rosetta Code http://rosettacode.org
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
parser = argparse.ArgumentParser(description='This program finds the best choice of Alitalia e-coupons in order to maximize the euro values.')

parser.add_argument('limit',  type=int, nargs='?', default=1000,
                   help='How many frequent flyer miles you have')
parser.add_argument('-t', choices='spa', default='s', nargs=1,
                    help='Type of the miles: standard (s) premium (p) all (a)')
parser.add_argument('-e', type=int,  nargs=1,
                    help='Max value of e-coupon to use (only e-coupons less or equal than this value are considered)')
parser.add_argument('-m', type=int,  nargs=1,
                    help='Max value of miles to use (only e-coupons that cost less or equal than this are considered)')


args = parser.parse_args()


standard = [
    ("20000s", 20000, 100),
    ("22000s", 22000, 110),
    ("24000s", 24000, 120),
    ("26000s", 26000, 130),
    ("28000s", 28000, 140),
    ("30000s", 30000, 150),
    ("32000s", 32000, 160),
    ("34000s", 34000, 170),
    ("36000s", 36000, 180),
    ("38000s", 38000, 190),
    ("40000s", 40000, 200),
    ("42000s", 42000, 210),
    ("44000s", 44000, 220),
    ("46000s", 46000, 230),
    ("48000s", 48000, 240),
    ("50000s", 50000, 250),
    ("54000s", 54000, 300),
    ("63000s", 63000, 350),
    ("72000s", 72000, 400),
    ("81000s", 81000, 450),
    ("90000s", 90000, 500),
    ("99000s", 99000, 550),
    ("108000s",108000,600),
    ("117000s",117000,650),
    ("126000s",126000,700),
    ("135000s",135000,750),
    ("144000s",144000,800),
    ("153000s",153000,850),
    ("162000s",162000,900),
    ("171000s",171000,950),
    ("180000s",180000,1000),
    ("220000s",220000,1500),
    ("260000s",260000,2000),
    ("300000s",300000,2500),
    ("340000s",340000,3000),
    ("370000s",370000,3500),
    ("400000s",400000,4000),
]

premium = [
    ("17000p", 17000  ,100),
    ("18700p", 18700  ,110),
    ("20400p", 20400  ,120),
    ("22100p", 22100  ,130),
    ("23800p", 23800  ,140),
    ("25500p", 25500  ,150),
    ("27200p", 27200  ,160),
    ("28900p", 28900  ,170),
    ("30600p", 30600  ,180),
    ("32300p", 32300  ,190),
    ("34000p", 34000  ,200),
    ("35700p", 35700  ,210),
    ("37400p", 37400  ,220),
    ("39100p", 39100  ,230),
    ("40800p", 40800  ,240),
    ("42500p", 42500  ,250),
    ("43200p", 43200  ,300),
    ("50400p", 50400  ,350),
    ("57600p", 57600  ,400),
    ("64800p", 64800  ,450),
    ("72000p", 72000  ,500),
    ("79200p", 79200  ,550),
    ("86400p", 86400  ,600),
    ("93600p", 93600  ,650),
    ("100800p", 100800 ,700),
    ("108000p", 108000 ,750),
    ("115200p", 115200 ,800),
    ("122400p", 122400 ,850),
    ("129600p", 129600 ,900),
    ("136800p", 136800 ,950),
    ("144000p", 144000 ,1000),
    ("180000p", 180000 ,1500),
    ("210000p", 210000 ,2000),
    ("240000p", 240000 ,2500),
    ("270000p", 270000 ,3000),
    ("300000p", 300000 ,3500),
    ("330000p", 330000 ,4000),
]

items = None
if args.t[0] == 's': items=standard
if args.t[0] == 'p': items=premium
if args.t[0] == 'a': items=standard+premium

if args.e: items = [i for i in items if i[2] <= args.e[0]]
if args.m: items = [i for i in items if i[1] <= args.m[0]]


def knapsack_unbounded_dp(items, C):
    # order by max value per item size
    items = sorted(items, key=lambda item: item[2]/float(item[1]), reverse=True)
 
    # Sack keeps track of max value so far as well as the count of each item in the sack
    sack = [(0, [0 for i in items]) for i in range(0, C+1)]   # value, [item counts]
 
    for i,item in enumerate(items):
        name, size, value = item
        for c in range(size, C+1):
            sackwithout = sack[c-size]  # previous max sack to try adding this item to
            trial = sackwithout[0] + value
            used = sackwithout[1][i]
            if sack[c][0] < trial:
                # old max sack with this added item is better
                sack[c] = (trial, sackwithout[1][:])
                sack[c][1][i] +=1   # use one more
 
    value, bagged = sack[C]
    numbagged = sum(bagged)
    size = sum(items[i][1]*n for i,n in enumerate(bagged))
    # convert to (iten, count) pairs) in name order
    bagged = sorted((items[i][0], n) for i,n in enumerate(bagged) if n)
 
    return value, size, numbagged, bagged

print "EUR: %d (miles %d). %d e-coupons: %s" % knapsack_unbounded_dp(items, args.limit)
